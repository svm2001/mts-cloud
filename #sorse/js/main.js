﻿$(function() {

    $('.header__search').click(function() {
        $('.search-form').toggleClass('active')
        return false
    });
    $('.search-form__clear').click(function() {
        $(this).parents('.search-form').removeClass('active')
    });
    $('.header__search').click(function() {
        $('.header__services').removeClass('active')
        $('.services-nav').slideUp('slow')
        return false
    });
    $('.nav-support').click(function() {
        $('.header__services').removeClass('active')
        $('.services-nav').slideUp('slow')
        return false
    });
    $('.header__services').click(function() {
        $(this).addClass('active')
        $('.services-nav').slideToggle('slow')
        return false
    });
    $('.services-nav__close').click(function() {
        $('.header__services').removeClass('active')
        $(this).parents('.services-nav').slideUp('slow')
        return false
    });


    $(".input-file input").change(function() {
        var thisFile = $(this).parent().find('.input-file__name');
        var text = 'Файл не выбран';
        thisFile.text(text).removeClass('active');
        if ($(this)[0].files.length == 1) {
            text = $(this)[0].files[0].name;
            thisFile.text(text).addClass('active');
        }

    });

    $(document).on('click', '.popup-close', function() {
        $.fancybox.close();
    })

    $(document).on('touchstart  click', '.popup-close', function() {
            $.fancybox.close();
    })

    $(document).on('mousedown', 'input', function() {
        if ($(this).parent().hasClass('error')) {
            $(this).parent().removeClass('error')
        }
    })


    var wow = new WOW({
        boxClass: 'wow', // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset: 100, // distance to the element when triggering the animation (default is 0)
        mobile: true, // trigger animations on mobile devices (default is true)
        live: true, // act on asynchronously loaded content (default is true)
        scrollContainer: null // optional scroll container selector, otherwise use window
    });
    wow.init();

    if ($(window).width() < 1200) {
        $('[data-wow-delay]').each(function() {
            $(this).addClass('animation-delay-none')
        })
    }



    $('.burger').click(function() {
        $(this).toggleClass('active');
        $('.mobile-nav').slideToggle('slow')
    });

    $('.mobile-nav__services h3').click(function() {
        $(this).toggleClass('serv-active').parent().find('ul').slideToggle('slow')
    });

    $('div.mobile-nav__title').click(function() {
        $(this).toggleClass('active').parent().find('.mobile-nav__hidden').slideToggle('slow')
    });


    // @prepros-append sliders.js
    // @prepros-append forms.js

    $('.s-about_top__item a').click((e) =>{
        e.preventDefault();
        let scrollClass = e.target.getAttribute('data-scroll')
        let scroll = document.getElementsByClassName(scrollClass)
        $('html, body').animate({
            scrollTop: $(scroll[0]).offset().top - 78
        }, 2000);
    })

    $('.mobile-nav__hidden a').click((e) =>{
        e.preventDefault();
        let scrollClass = e.target.getAttribute('data-scroll')
        let scroll = document.getElementsByClassName(scrollClass)
        $('html, body').animate({
            scrollTop: $(scroll[0]).offset().top - 78
        }, 2000);
    })
});




/*
    $("input[name=phone]").mask("+7 (999) 999-99-99");

    var  wheihht=96;
    $(window).scroll(function(){
        var bo = $(this).scrollTop();
        if ( bo >= wheihht) {$('nav').addClass('scr_nav')};
            if ( bo < wheihht) {$('nav').removeClass('scr_nav')};
    })
    $("nav li a").click(function(){
        var target = $(this).attr('href');
            $('html, body').animate({scrollTop: $(target).offset().top-20}, 1300);
            return false;
    });

*/


$(function() {
    let fixed = false
    let wt = $(window).scrollTop();
    let wh = $(window).height();
    let et = $('.s-about_top').offset().top;
    let eh = $('.s-about_top').outerHeight();
    let dh = $(document).height();

    $(document).ready(function() {
        if(wt > et-eh) {
            fixed = true
        }
        if(wt < et+eh) {
            fixed = false
        }
        if(fixed){
            $('body').addClass('fixed-top');
        } else {
            $('body').removeClass('fixed-top');
        }
    });

    $(window).scroll(function(){
        let wt = $(window).scrollTop();
        if(wt > et-eh){
            fixed = true
        }

        if(wt < et+eh) {
            fixed = false
        }
        if(fixed){
            $('body').addClass('fixed-top');
        } else {
            $('body').removeClass('fixed-top');
        }
    });
});
$(function configurate () {
    let decrease = $('.js-configurator__remove');
    let increase = $('.js-configurator__add');
    let total = $('.js-configurator__total');
    let allSum = $('.js-configurator__sum');
    let resetBtn = $('.js-configurator__reset');
    let allCounting = $('.js-configurator__counting');
    
    
    $(increase).on('click', function () {
        let line = this.closest('.s-configurator__line');
        let counting = $(line).find('.js-configurator__counting');
        let price = Number($(line).find('.js-configurator__price')[0].textContent);
        let sum = Number($(line).find('.js-configurator__sum')[0].textContent);
        
        counting[0].textContent++
        sum = sum + price
        $(line).find('.js-configurator__sum')[0].textContent = sum
        count()
    })
    $(decrease).on('click', function () {
        let line = this.closest('.s-configurator__line');
        let counting = $(line).find('.js-configurator__counting');
        let price = Number($(line).find('.js-configurator__price')[0].textContent);
        let sum = Number($(line).find('.js-configurator__sum')[0].textContent);
        if (counting[0].textContent > 0) {
            counting[0].textContent--
            sum = sum - price
            $(line).find('.js-configurator__sum')[0].textContent = sum
            count()
        }
    })
    $(resetBtn).on('click', function () {
        for (let i = 0; i < allCounting.length; i++) {
            allCounting[i].textContent = 0
        }
        for (let j = 0; j < allSum.length; j++) {
            allSum[j].textContent = 0
        }
        total[0].textContent = 0
        $('.js-configurator__yes').removeClass('active');
        $('.js-configurator__no').addClass('active');
    })
    $('.js-configurator__yes').on('click', function () {
        if (!$(this).hasClass('active')) {
            $(this).addClass('active');
            $('.js-configurator__no').removeClass('active');
            let line = this.closest('.s-configurator__line');
            $(line).find('.js-configurator__sum')[0].textContent = $(line).find('.js-configurator__price')[0].textContent;
            count()
        }
    })
    $('.js-configurator__no').on('click', function () {
        if (!$(this).hasClass('active')) {
            $(this).addClass('active');
            $('.js-configurator__yes').removeClass('active');
            let line = this.closest('.s-configurator__line');
            $(line).find('.js-configurator__sum')[0].textContent = 0;
            count()
        }
    })
    function count () {
        total[0].textContent = 0;
        for (let i = 0; i < allSum.length; i++) {
            total[0].textContent = Number(total[0].textContent) + Number(allSum[i].textContent.replace(/ /g, ""))
        }
        total[0].textContent = numberWithSpaces(total[0].textContent)
    }
    function numberWithSpaces(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    }
})
var acc = document.getElementsByClassName("accordion");
var i;
for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
            panel.classList.remove("active");
            // panel.style.padding = 0;
        } else {
            panel.classList.add("active");
            panel.style.maxHeight = panel.scrollHeight + "px";
            // panel.style.padding = '10px 18px';
            // panel.style.paddingLeft = '80px';
        }
    });
}

// @prepros-append external/jquery.overlayScrollbars.js

$(function () {

    var options2 = {
        overflowBehavior: {
            x: 'scroll',
            y: 'hidden',
            clipAlways: false
        },
        scrollbars : {
            visibility: 'hidden',
        }
    };
    
    $('.js-scroll-x').overlayScrollbars(options2);
    
})

function MediaHandler(queriesList, func) {
    if (typeof func !== 'function') {
        func = function (f) {
            if (typeof f === 'function') {
                f();
            }
        };
    }
    var defaultQuery;
    var queryList;
    var defaultValue = queriesList.default;
    var queries = $.extend({}, queriesList);
    delete queries.default;
    defaultQuery = {query: 'default', value: defaultValue};
    queryList = Object.entries(queries)
        .map(function (arr) {
            var query = arr[0];
            var value = arr[1];
            return {list: window.matchMedia(query), value: value, query: query};
        });
    queryList.forEach(function (object) {
        object.list.addListener(setCurrentQuery);
    });
    setCurrentQuery();
    this.destroy = function () {
        queryList.forEach(function (object) {
            object.list.removeListener(setCurrentQuery);
        });
        queryList = null;
    };

    function setCurrentQuery() {
        var current = queryList.filter(function (object) {
            return object.list.matches;
        })[0] || defaultQuery;
        func(current.value, current.query);
    }
}

$('.select-filter__title').click(function() {
    $(this).parent().toggleClass('active').find('.select-filter__body').slideToggle('slow').parents('.price-configurator-filter__item').toggleClass('active')
});

$('.select-filter__item').click(function() {
    var val = $(this).text()
    $(this).parent().slideUp('slow').parent().removeClass('active').find('.select-filter__title').removeClass('active').text(val).parents('.price-configurator-filter__item').removeClass('active');
});

$('.js-tab-trigger').click(function () {
    var id = $(this).attr('data-tab');
    var content = $('.js-tab-content[data-tab="'+ id +'"]');

    $('.js-tab-trigger.active').removeClass('active');
    $(this).addClass('active');

    $('.js-tab-content.active').removeClass('active');
    content.addClass('active');
});